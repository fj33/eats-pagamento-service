package br.com.caelum.eats.pagamento;

import java.net.URI;

import java.util.ArrayList;
import java.util.List;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.AllArgsConstructor;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/pagamentos")
@AllArgsConstructor
class PagamentoController {

    private PagamentoRepository pagamentoRepository;
    private PedidoRestClient pedidoClient;

    @GetMapping("/{id}")
    EntityModel<PagamentoDto> detalha(@PathVariable("id") Long id) {
	Pagamento pagamento = pagamentoRepository.findById(id).orElseThrow(ResourceNotFoundException::new);

	List<Link> links = new ArrayList<>();
	Link self = linkTo(methodOn(PagamentoController.class).detalha(id)).withSelfRel();
	links.add(new LinkWithMethod(self, HttpMethod.GET.name()));

	if (Pagamento.Status.CRIADO.equals(pagamento.getStatus())) {
	    Link confirma = linkTo(methodOn(PagamentoController.class).confirma(id)).withRel("confirma");
	    links.add(new LinkWithMethod(confirma, HttpMethod.PUT.name()));
	    Link cancela = linkTo(methodOn(PagamentoController.class).cancela(id)).withRel("cancela");
	    links.add(new LinkWithMethod(cancela, HttpMethod.DELETE.name()));
	}

	PagamentoDto pagamentoDto = new PagamentoDto(pagamento);
	EntityModel<PagamentoDto> resource = new EntityModel<>(pagamentoDto, links);
	return resource;
    }

    @PostMapping
    ResponseEntity<EntityModel<PagamentoDto>> cria(@RequestBody Pagamento pagamento, UriComponentsBuilder uriBuilder) {
	pagamento.setStatus(Pagamento.Status.CRIADO);
	Pagamento pagamentoSalvo = pagamentoRepository.save(pagamento);

	URI path = uriBuilder.path("/pagamentos/{id}").buildAndExpand(pagamentoSalvo.getId()).toUri();

	PagamentoDto pagamentoDto = new PagamentoDto(pagamentoSalvo);
	Long id = pagamentoSalvo.getId();

	List<Link> links = new ArrayList<>();
	Link self = linkTo(methodOn(PagamentoController.class).detalha(id)).withSelfRel();
	links.add(new LinkWithMethod(self, HttpMethod.GET.name()));

	Link confirma = linkTo(methodOn(PagamentoController.class).confirma(id)).withRel("confirma");
	links.add(new LinkWithMethod(confirma, HttpMethod.PUT.name()));

	Link cancela = linkTo(methodOn(PagamentoController.class).cancela(id)).withRel("cancela");
	links.add(new LinkWithMethod(cancela, HttpMethod.DELETE.name()));

	EntityModel<PagamentoDto> resource = new EntityModel<>(pagamentoDto, links);
	return ResponseEntity.created(path).body(resource);
    }

    @PutMapping("/{id}")
    EntityModel<PagamentoDto> confirma(@PathVariable("id") Long id) {
	Pagamento pagamento = pagamentoRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
	pagamento.setStatus(Pagamento.Status.CONFIRMADO);
	pedidoClient.avisaQueFoiPago(pagamento.getPedidoId());
	pagamentoRepository.save(pagamento);

	pedidoClient.avisaQueFoiPago(pagamento.getPedidoId());

	List<Link> links = new ArrayList<>();
	Link self = linkTo(methodOn(PagamentoController.class).detalha(id)).withSelfRel();
	links.add(new LinkWithMethod(self, HttpMethod.GET.name()));

	PagamentoDto dto = new PagamentoDto(pagamento);
	EntityModel<PagamentoDto> resource = new EntityModel<>(dto, links);
	return resource;
    }

    @DeleteMapping("/{id}")
    EntityModel<PagamentoDto> cancela(@PathVariable("id") Long id) {
	Pagamento pagamento = pagamentoRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
	pagamento.setStatus(Pagamento.Status.CANCELADO);
	pagamentoRepository.save(pagamento);

	List<Link> links = new ArrayList<>();
	Link self = linkTo(methodOn(PagamentoController.class).detalha(id)).withSelfRel();
	links.add(self);

	PagamentoDto pagamentoDto = new PagamentoDto(pagamento);
	EntityModel<PagamentoDto> resource = new EntityModel<>(pagamentoDto, links);
	return resource;
    }

}